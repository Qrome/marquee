#pragma once
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

class GeoNamesClient {

private:
  String myLat = "";
  String myLon = "";
  String myUserName = "";
  
  const char* servername = "api.geonames.org";  // remote server we will connect to

public:
  GeoNamesClient(String UserName, String lat, String lon);
  float getTimeOffset();

};

