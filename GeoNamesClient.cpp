#include "GeoNamesClient.h"

GeoNamesClient::GeoNamesClient(String UserName, String lat, String lon) {
  myLat = lat;
  myLon = lon;
  myUserName = UserName;
}

float GeoNamesClient::getTimeOffset() {
  WiFiClient client;
  String apiGetData = "GET /timezoneJSON?lat=" + myLat + "&lng=" + myLon + "&username=" + myUserName + " HTTP/1.1";

  Serial.println("Getting TimeZone Data for " + myLat + "," + myLon);
  Serial.println(apiGetData);
  String result = "";
  if (client.connect(servername, 80)) {  //starts client connection, checks for connection
    client.println(apiGetData);
    client.println("Host: " + String(servername));
    client.println("User-Agent: ArduinoWiFi/1.1");
    client.println("Connection: close");
    client.println();
  } 
  else {
    Serial.println("connection for timezone data failed"); //error message if no client connect
    Serial.println();
    return 0;
  }

  while(client.connected() && !client.available()) delay(1); //waits for data
 
  Serial.println("Waiting for data");

  boolean record = false;
  while (client.connected() || client.available()) { //connected or data available
    char c = client.read(); //gets byte from ethernet buffer
    if (String(c) == "{") {
      record = true;
    }
    if (record) {
      result = result+c; 
    }
    if (String(c) == "}") {
      record = false;
    }
  }
  client.stop(); //stop client
  Serial.println(result);

  char jsonArray [result.length()+1];
  result.toCharArray(jsonArray,sizeof(jsonArray));
  jsonArray[result.length() + 1] = '\0';
  DynamicJsonBuffer json_buf;
  JsonObject& root = json_buf.parseObject(jsonArray);
  String offset = (const char*)root["dstOffset"];
  Serial.println("dstOffset for " + String((const char*)root["timezoneId"]) + " is: " + offset);
  Serial.println();
  return offset.toFloat();
}




