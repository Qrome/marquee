#pragma once
#include <ESP8266WiFi.h>
#include <JsonListener.h>
#include <JsonStreamingParser.h>

class NewsApiClient: public JsonListener {

  private:
    String mySource = "";
    String myApiKey = "";
    String mySortBy = "";
    
    String currentKey = "";
    int counterTitle = 0;

    typedef struct {
      String title;
      String description;
      String url;
    } newsfeed;

    newsfeed news[10];
    
    const char* servername = "newsapi.org";  // remote server we will connect to
  
  public:
    NewsApiClient(String ApiKey, String NewsSource, String SortBy);
    void updateNews();
    void updateNewsSource(String source);
    
    String getTitle(int index);
    String getDescription(int index);
    String getUrl(int index);
    String cleanText(String text);
    
    virtual void whitespace(char c);
    virtual void startDocument();
    virtual void key(String key);
    virtual void value(String value);
    virtual void endArray();
    virtual void endObject();
    virtual void endDocument();
    virtual void startArray();
    virtual void startObject();

};

