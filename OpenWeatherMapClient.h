#pragma once
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

class OpenWeatherMapClient {

private:
  String myCityIDs = "";
  String myApiKey = "";
  String units = "";
  
  const char* servername = "api.openweathermap.org";  // remote server we will connect to
  String result;

  typedef struct {
    String lat;
    String lon;
    String dt;
    String city;
    String country;
    String temp;
    String humidity;
    String condition;
    String wind;
    String weatherId;
    String description;
    String icon;
    boolean cached;
  } weather;

  weather weathers[5];

  
  
public:
  OpenWeatherMapClient(String ApiKey, int CityIDs[], int cityCount, boolean isMetric);
  void updateWeather();
  void updateCityIdList(int CityIDs[], int cityCount);
  void setMetric(boolean isMetric);

  String getWeatherResults();

  String getLat(int index);
  String getLon(int index);
  String getDt(int index);
  String getCity(int index);
  String getCountry(int index);
  String getTemp(int index);
  String getHumidity(int index);
  String getCondition(int index);
  String getWind(int index);
  String getWeatherId(int index);
  String getDescription(int index);
  String getIcon(int index);
  boolean getCached();
  String getMyCityIDs();
  String getWeatherIcon(int index);
};

