#include "NewsApiClient.h"

#define arr_len( x )  ( sizeof( x ) / sizeof( *x ) )

NewsApiClient::NewsApiClient(String ApiKey, String NewsSource, String SortBy) {
  
  mySource = NewsSource;
  myApiKey = ApiKey;
  mySortBy = SortBy;

}

void NewsApiClient::updateNews() {
  JsonStreamingParser parser;
  parser.setListener(this);
  WiFiClient newsClient;

  String apiGetData = "GET /v1/articles?source=" + mySource + "&sortBy=" + mySortBy + "&apiKey=" + myApiKey + " HTTP/1.1";

  Serial.println("Getting News Data");
  Serial.println(apiGetData);

  if (newsClient.connect(servername, 80)) {  //starts client connection, checks for connection
    newsClient.println(apiGetData);
    newsClient.println("Host: " + String(servername));
    newsClient.println("User-Agent: ArduinoWiFi/1.1");
    newsClient.println("Connection: close");
    newsClient.println();
  } 
  else {
    Serial.println("connection for news data failed: " + String(servername)); //error message if no client connect
    Serial.println();
    return;
  }
  
  while(newsClient.connected() && !newsClient.available()) delay(1); //waits for data
 
  Serial.println("Waiting for data");

  int size = 0;
  char c;
  boolean isBody = false;
  while (newsClient.connected() || newsClient.available()) { //connected or data available
    c = newsClient.read(); //gets byte from ethernet buffer
    if (c == '{' || c == '[') {
      isBody = true;
    }
    if (isBody) {
      parser.parse(c);
    }
  }
  newsClient.stop(); //stop client
}

String NewsApiClient::getTitle(int index) {
  return news[index].title;
}

String NewsApiClient::getDescription(int index) {
  return news[index].description;
}

String NewsApiClient::getUrl(int index) {
  return news[index].url;
}

void NewsApiClient::updateNewsSource(String source) {
  mySource = source;
}

void NewsApiClient::whitespace(char c) {

}

void NewsApiClient::startDocument() {
  counterTitle = 0;
}

void NewsApiClient::key(String key) {
  currentKey = key;
}

void NewsApiClient::value(String value) {
  if (counterTitle == 10) {
    // we are full so return
    return;
  }
  if (currentKey == "title") {
    news[counterTitle].title = cleanText(value);
  }
  if (currentKey == "description") {
    news[counterTitle].description = cleanText(value);
  }
  if (currentKey == "url") {
    news[counterTitle].url = value;
    counterTitle++;
  }
  Serial.println(currentKey + "=" + value);
}

void NewsApiClient::endArray() {
}

void NewsApiClient::endObject() {
}
void NewsApiClient::startArray() {
}

void NewsApiClient::startObject() {
}

void NewsApiClient::endDocument() {
}

String NewsApiClient::cleanText(String text) {
  text.replace("’", "'");
  text.replace("“", "\"");
  text.replace("”", "\"");
  text.replace("`", "'");
  text.replace("‘", "'");
  text.replace("\\\"", "'");
  text.replace("•", "-");
  return text;
}




